import React, { Component } from 'react'
import './stationlist.css'

const Connections = ({ connections }) => {
  if (connections.length === 0) return ""
  // This does not query for station name, just displays its numerical id
  return (<ol class="connectionList">
    {connections.map((conn) => {
      return (<li>{conn.destination_id} {conn.train} {conn.departure}</li>)
    })}
  </ol>
  );
}

class StationList extends Component<{}, State> {
  render() {
    return (<ul className='stationList'>
      {this.renderStations()}
    </ul>)
  }

  stationClicked(id, name) {
    this.props.handleStation(id, name)
  }

  renderStations() {
    const stations = this.props.stations

    return stations.map((obj) => (
      <li key={obj.id} onClick={() => this.stationClicked(obj.id, obj.name)}>
        {obj.name}
      </li>)
    )
  }
}

class StationInput extends Component<{}, State> {
  state = {
    stations: [],
    stationId: -1,
    connections: []
  }

  lookupStation(text) {
    if (text.length >= 4) {
      fetch(`https://api-v3.mojepanstwo.pl/dane/kolej_stacje?conditions[q]=${text}`)
        .then((response) => {
          // fetch() responses don't decode their JSON immediately, this requires another step
          response.json().then((obj) => {
            if (obj.Count > 0)
              this.parseStations(obj.Dataobject)
          })
        })
    }
  }

  parseStations(list) {
    var stations = list.map((station) => {
      const id = station.id
      const name = station.data["kolej_stacje.nazwa"]
      return {id: id, name: name}
    })
    this.setState({ stations: stations, connections: [], stationId: -1 })
  }

  filterMapResults(station_id, results) {
    // Reject results that don't start at this given station.
    // Then transform them to a usable form
    return results.filter(obj => obj.data["kolej_linie.start_stacja_id"] == station_id)
      .map(obj => ({
        destination_id: obj.data["kolej_linie.stop_stacja_id"],
        departure: obj.data["kolej_linie.czast_start"],
        train: obj.data["kolej_linie.nazwa"],
        key: obj.global_id
      }))
  }

  /* This is the most complex part. As this is a paginated resource, we need to repeatedly query,
   * and resolve only on reaching the last page. We do this by recursively calling ourselves,
   * with the acc parameter containing successively growing list of results.
   */
  loadSchedulePage(station_id, url, lastUrl, acc, resolve, reject) {
    fetch(url)
      .then(response => {
        response.json().then(data => {
          let toAdd = this.filterMapResults(station_id, data.Dataobject)
          let newAcc = acc.concat(toAdd)
          if (data.Links.next && data.Links.last)
            // This is the first page, it has a next and last pointer
            // DecodeURI is necessary, because the urls are returned encoded, and querying them as-is returns no results
            this.loadSchedulePage(station_id, decodeURI(data.Links.next), decodeURI(data.Links.last), newAcc, resolve, reject)
          else {
            if (url == lastUrl)
              // We are done!
              resolve(newAcc)
            else {
              // This is one of the following pages.
              // To calculate the next page, increment the trailing page number param
              let currentPage = Number(url.match(/page=(\d+)$/)[1])
              let nextUrl = url.replace(/page=(\d+)$/, `page=${currentPage + 1}`)
              this.loadSchedulePage(station_id, nextUrl, lastUrl, newAcc, resolve, reject)
            }
          }
        })
      })
  }

  loadSchedule(stationId, stationName) {
    this.setState({stations: [], stationId: stationId})
    let url = `https://api-v3.mojepanstwo.pl/dane/kolej_linie?conditions[q]=${stationName}`
    new Promise((resolve, reject) => {
      this.loadSchedulePage(stationId, url, null, [], resolve, reject)
    }).then(result => {
      this.setState(prev => ({...prev, connections: result}))
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.connections.length > 0) {
      this.mapLookup(this.state.connections).then((data) => {
        this.props.connectionsFound(data)
      })
    }
  }

  mapLookup(connections) {
    const uniq = (value, index, self) => self.indexOf(value) === index
    let root = 'https://api-v3.mojepanstwo.pl/dane/kolej_stacje'
    let station_ids = [this.state.stationId, ...connections.map((conn) => conn.destination_id)].filter(uniq)
    let promises = station_ids.map((sid) => fetch(`${root}/${sid}`))
    let promise = new Promise((resolve, reject) => {
      Promise.all(promises).then((result) => {
        // result is an array of fetch() promises, for which we still need to get json
        Promise.all(result.map((resp) => resp.json())).then((json_results) => {
          resolve(json_results.map((obj) => ({ name: obj.data["kolej_stacje.nazwa"], lat: obj.data["kolej_stacje.loc_lat"], lng: obj.data["kolej_stacje.loc_lng"] })))
        })
      })
    })
    return promise
  }

  keydown(event) {
    /* Handle the Enter key.
       An improvement would be to handle any key, and set a timeout for e.g. 300ms of inaction,
       and do the lookup then. */
    if (event.keyCode === 13)
      this.lookupStation(event.target.value)
    return false
  }

  render() {
    return (
      <div className="StationInput">
        <label htmlFor="station_input">Stacja</label>
        <input type="text" id="station_input" onKeyDown={(event) => this.keydown(event)}/>
        <StationList stations={this.state.stations} handleStation={this.loadSchedule.bind(this)}/>
        <Connections connections={this.state.connections}/>
      </div> 
    )
  }
}

export default StationInput;
