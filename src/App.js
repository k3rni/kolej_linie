import React, { Component, Fragment } from 'react';
import './App.css';
import 'leaflet/dist/leaflet.css'
import L from 'leaflet'
import { Map, WMSTileLayer, Marker, Polyline } from 'react-leaflet';
import StationInput from './components/StationInput'

import icon from 'leaflet/dist/images/marker-icon.png'
import iconShadow from 'leaflet/dist/images/marker-shadow.png'

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
	iconAnchor: [12, 40]
});

class App extends Component<{}, State> {
  state = {
    lat: 52.232,
    lng: 21.007,
    zoom: 8,
    hubLocation: null,
    stationLocations: [],
  }

  connectionsFound(locations) {
    var hubLocation, stationLocations 
    [hubLocation, ...stationLocations] = locations
    this.setState(prevState => {
      return {...prevState, hubLocation: hubLocation, stationLocations: stationLocations}
    })
  }

  renderStationMarkers(hub, stations) {
    if (!hub) return ""
    let locations = [hub, ...stations]
    return locations.map((loc) => (<Marker key={loc.name} position={[loc.lat, loc.lng]} icon={DefaultIcon}/>))
  }

  /* An example path function. Generates a two-segment path, of which one is always horizontal, and the other vertical */
  polyPath(start, finish) {
    let path = [[start.lat, start.lng]]
    path.push([Math.min(Number(start.lat), Number(finish.lat)), start.lng])
    path.push([Math.min(Number(start.lat), Number(finish.lat)), finish.lng])
    path.push([finish.lat, finish.lng])
    return path
  }

  renderPolylines(hub, stations) {
    if (!hub) return ""
    return stations.map((loc) => {
      return (<Polyline positions={this.polyPath(hub, loc)}/>)
    })
  }


  render() {
    const position = [this.state.lat, this.state.lng]
    return (
      <div className="App">
        <StationInput connectionsFound={this.connectionsFound.bind(this)}/>
        <Map center={position} zoom={this.state.zoom} style={{width: '100vw', height: '100vh'}}>
          <WMSTileLayer
            url='https://mapy.geoportal.gov.pl/wss/service/img/guest/Ogolnogeograficzna/MapServer/WMSServer?'
            layers='Ogolnogeograficzna'
            format='image/png'
            attribution='geoportal.gov.pl'
          />
          <Fragment>{this.renderStationMarkers(this.state.hubLocation, this.state.stationLocations)}</Fragment>
          <Fragment>{this.renderPolylines(this.state.hubLocation, this.state.stationLocations)}</Fragment>
        </Map>
      </div>
    );
  }
}

export default App;
